from pymongo import MongoClient
import urllib3
from bson.code import Code
from datetime import datetime
import hashlib
import pytz
local = pytz.timezone ("Australia/Brisbane")

client = MongoClient("10.152.0.3",27017)
def mapR():
    
    db = client.tweets
    collection = db.djias
    map = Code("function(){emit(this.id, new Date(Date.parse(this.status.created_at.replace(/(\+\S+) (.*)/, '$2 $1'))));}")
    reduce = Code("function(key, values){ return values; }")
    collection.map_reduce(map, reduce, "djias2_datetime")
    
def main():

    utc_date = datetime.utcnow()
    date = datetime.now()
    
    logs = client.BatchLog
    log_col = logs.map_reducer_log
    id_ = date.strftime("%Y-%m-%d %H:%M:%S")
    m = hashlib.sha256(id_.encode("utf-8"))
    m = m.hexdigest()

    try:
        mapR()
        
        d = dict()
        d["updated_at"] = date
        d["updated_at_utc"]= utc_date
        d["status"]= "Success"
        d["message"] = "Map Reduce Run"
        d["id"] = m
        log_col.insert(d)
       
        client.close()
    except Exception as e:
        
        d = dict()
        d["updated_at"] = utc_date
        d["updated_at_utc"]= utc_date
        d["status"]= "Failed"
        d["message"] = str(e)
        d["id"] = m
        log_col.insert(d)
        client.close()

# main()