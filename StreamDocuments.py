from pymongo import MongoClient, DESCENDING
import urllib3
from bson.code import Code
from datetime import datetime
import hashlib
import pytz
from kafka import SimpleProducer, KafkaClient
import json
import time
import uuid
import DocumentMapping

local = pytz.timezone ("Australia/Brisbane")

client = MongoClient("10.152.0.3",27017)
db = client.tweets
collection = db.djias2_datetime

logs = client.BatchLog
log_col = logs.map_reducer_log

kafka = KafkaClient('localhost:9092')
producer = SimpleProducer(kafka)
topic = 'test'
print ("emitting ...")

utc_now = datetime.utcnow()
now = datetime.now()

try:

    DocumentMapping.main()

    u = uuid.uuid4()

    kafka_log_col = logs.kafka_producer_log

    if (log_col.find({}).count()>0):
        log = log_col.find({}).sort([("updated_at",DESCENDING)])[1]
        print (log)
        last_update = log["updated_at_utc"]
        # last_update = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
        last_update_utc = local.localize(last_update, is_dst=None)
        last_update_utc = last_update_utc.astimezone(pytz.utc)
        print (last_update)
        print (last_update_utc)
        print (utc_now)
        now_update_utc = utc_now
        
    else:
        last_update = datetime(2018, 1, 25, 00, 00,00,00)
        now_update_utc = datetime(2018, 1, 31, 00,00,00,00)
        # loc = local.localize(last_update, is_dst=None)
        # last_update = loc.astimezone(pytz.utc)

    # log = log_col.find({}).sort([{"updated_at",DESCENDING}])[0]
    
    

    # print (datetime.utcnow())
    # for testing sake
    symbol = "GE"
    symbols = [symbol, "MCD","IBM","MMM","WMT","XOM","CAT","F","GM","CVX","BA","AXP","NKE"]
    # symbols = [symbol]
    mapR = collection.find({"$and":[{"value":{"$gte":last_update}},{"value":{"$lt":now_update_utc}}]})
    # mapR = collection.find({"$and":[{"value":{"$gte":last_update}},{"value":{"$lt":utc_now}}]})

    tweets = db.djias
    
    for i in mapR:
        # print (i["_id"])
        t = tweets.find_one({"id":i["_id"], "$or":[{"tickers.status":{"$in": symbols}},{"tickers.quoted_status":{"$in":symbols}} ]})
        # print (t["status"]["text"])
        if t == None:
            continue
        # print (t)
        doc = dict()
        doc["status"] = t["status"]
        doc["id"] = t["id"]
        doc["tickers"] = t["tickers"]
        doc["uuid"] = u.hex
        doc["created_at"] = now.strftime("%Y-%m-%d %H:%M:%S")
        doc = json.dumps(doc)
        producer.send_messages(topic,doc.encode("utf-8"))
        time.sleep(0.2)

    print (mapR.count())

    d = dict()
    d["updated_at"] = now
    d["updated_at_utc"] = utc_now
    d["status"]="Successful stream"
    d["message"] = "Kafka messages sent. Size:" + str(mapR.count())
    
    kafka_log_col.insert(d)

except Exception as e:

    d = dict()
    d["updated_at"] = now
    d["updated_at_utc"] = utc_now
    d["status"]="Failed stream"
    d["message"] = str(e)
    
    kafka_log_col.insert(d)


